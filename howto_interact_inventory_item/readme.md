## Interact with InventoryItem

When interacting with any other object than inventory items the interaction is very simple.
You just grab the object you want and call interactWith with the action name.

When it comes to inventory items this is more complicated.
This is because currently I can't figure out the action id for inventory items.

Therefore calls to interactWith(InventoryItem, ... ) look like the following:
interactWith(item, action_id, action name)

So what is this action id?
It is a unique action id for this specific action.
This id is the same across all objects.
For example Use action is action id 38.

So how do you find this id?

1: First make sure you have your inventory item in the inventory.
2: Next open Data View in OSRS Emu Bot.
3: Open the getInventoryActionLog node.
4: Perform the inventory action you are interested in.
5: Now a new number should pop up in getInventoryActionLog.
6: This is the id you want to use.