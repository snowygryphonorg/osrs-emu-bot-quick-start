import com.snowygryphon.osrs.script.Character;
import com.snowygryphon.osrs.script.NPC;
import com.snowygryphon.osrs.script.Script;
import com.snowygryphon.osrs.script.ScriptInfo;
import org.joml.Vector2i;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Stream;

@ScriptInfo(name = "Kill Chickens", author = "SnowyGryphon", version = "1.0")
public class KillChickens extends Script {
    Character currentChicken;

    @Override
    public void update() {
        if(currentChicken == null) {
            Stream<NPC> chickens = getChickens();
            currentChicken = chickens.findFirst().orElse(null);
        }

        if(currentChicken != null) {
            if(!currentChicken.isValid() || currentChicken.getAttackTarget() != null && currentChicken.getAttackTarget() != bot.getLocalPlayer()) {
                currentChicken = null;
            } else {
                bot.interactWith(currentChicken, "Attack");
            }
        }
    }

    private Stream<NPC> getChickens() {
        Vector2i local_tile = bot.getLocalPlayer().getTile();

        return bot.getNPCs().stream()
                .filter(n -> n.getName().equals("Chicken") && n.getAttackTarget() == null) // Is chicken and is not in combat
                .sorted(Comparator.comparingDouble(a -> a.getTile().distance(local_tile))); // Get the closest chicken
    }
}
