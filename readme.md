# Quick Start OSRS Emu Bot

This is quick start guide on how to create a script for [OSRS Emu Bot](https://snowygryphon.com/products/osrs_emu_bot).
If you are looking for docs go to
https://snowygryphon.com/docs/osrs_emu_bot/index.html

## Required tools
tools needed:
    
- JDK 13 or above [download](https://www.oracle.com/java/technologies/javase-jdk16-downloads.html)
- OSRS Emu Bot [download](https://snowygryphon.com/download#osrs_emu_bot)
- Intellij [download](https://www.jetbrains.com/idea/download/)
- Windows 7 or above
    
If you don't want to use intellij the basic layout is the following:
    
1: reference jar %USERPROFILE%/osrs_emu_bot/osrs_emu_bot.jar
 
2: compile the jar and output it into %USERPROFILE%/osrs_emu_bot/scripts

The following will be a specific guide on how to setup intellij.

## Create project

Create a new blank java project without any dependency manager,
for this project we don't need one.

Use the following setup detailed in the images below:

![alt-text](images/new_project.png)
![alt-text](images/create_from_template.png)
![alt-text](images/project_location.png)

Then hit finish and you are done. You now got your project.

## Reference osrs_emu_bot jar

Open project structure settings:

![alt-text](images/open_project_structure.png)

Under libraries add a library reference to C:\Users\USER_NAME\osrs_emu_bot\osrs_emu_bot.jar
so that is looks something like this:

![alt-text](images/reference_library.png)

## Output the jar into the scripts directory

Open project structure if you do not already have it open.
Next open the artifacts tab.

![alt-text](images/artifacts_tab.png)

Create new artifact.

![alt-text](images/create_artifact.png)

Next set the output directory to C:\Users\USER_NAME\osrs_emu_bot\scripts

![alt-text](images/output_dir.png)

Set the checkbox Include in project build.

![alt-text](images/include_in_project_build.png)

Make our project part of artifact.

![alt-text](images/compile_output_to_be_done.png)
--
![alt-text](images/compile_output_step.png)
--
![alt-text](images/compile_output_done.png)

Now click ok, you should now have a good setup.

Build project.

![alt-text](images/build.png)

You can now start coding.



## Our first script 

Start OSRS Emu bot, and login.

Create a new script under the src directory named KillChickens with the following code:

    import com.snowygryphon.osrs.script.Script;
    import com.snowygryphon.osrs.script.ScriptInfo;

    @ScriptInfo(name = "Kill Chickens", author = "SnowyGryphon", version = "1.0")
    public class KillChickens extends Script {
        @Override
        public void update() {
            bot.logInfo("Hello");
        }
    }
    
Build your project.
Open OSRS Emu Bot window and you should see a script coming up named Kill Chickens.

![alt-text](images/osrs_emu_bot.png)

All the methods, data and available operations is done by accessing the bot field.
All data is a snapshot of the world of what it was before entering the update method.
If you need another snapshot in the middle of the update method bot.updateData() is the appropriate thing to call.
Otherwise you could potentially be working with old data.

Let's create a simple script which tries to kill all the chickens nearby.

First create a method which get all the nearby chickens and orders them by distance to the local player.

    private Stream<NPC> getChickens() {
        Vector2i local_tile = bot.getLocalPlayer().getTile();

        return bot.getNPCs().stream()
                .filter(n -> n.getName().equals("Chicken") && n.getAttackTarget() == null) // Is chicken and is not in combat
                .sorted(Comparator.comparingDouble(a -> a.getTile().distance(local_tile))); // Get the closest chicken
    }
    
Next we need a field to keep track of the currently attacked chicken.

    Character currentChicken;

With all the data required we can start working with the update method.

First find a new chicken if we are not attacking any:

    if(currentChicken == null) {
        Stream<NPC> chickens = getChickens();
        currentChicken = chickens.findFirst().orElse(null);
    }

Next attack the current chicken if are not currently attacking it.

    if(currentChicken != null) {
        if(!currentChicken.isValid() || currentChicken.getAttackTarget() != null && currentChicken.getAttackTarget() != bot.getLocalPlayer()) {
            currentChicken = null;
        } else {
            bot.interactWith(currentChicken, "Attack");
        }
    }
    
Now we have our first little script.
Of course this small script does not handle gates and doors but that is out of scope for this quick start guide.

Happy coding and have a nice day :)

For full source code you can check out the src directory which contains the KillChickens class.